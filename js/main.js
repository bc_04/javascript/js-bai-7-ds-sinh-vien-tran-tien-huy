let getScoreFromTr = (trTag) => {
  return trTag.querySelectorAll("td")[3].innerText * 1;
};

let getNameFromTr = (trTag) => {
  return trTag.querySelectorAll("td")[2].innerText;
};

let find_max_by_index = (data_rows) => {
  let max_index = 0;
  data_rows.forEach((item, index) => {
    getScoreFromTr(item) > getScoreFromTr(data_rows[max_index]) &&
      (max_index = index);
  });

  return max_index;
};

let find_min_by_index = (data_rows) => {
  let min_index = 0;
  data_rows.forEach((item, index) => {
    getScoreFromTr(item) < getScoreFromTr(data_rows[min_index]) &&
      (min_index = index);
  });

  return min_index;
};

let get_student_mark_greater_than_x = (data_scores_arr, mark) =>
  data_scores_arr.filter((item) => getScoreFromTr(item) > mark);

let sort_asceding = (data_rows) => {
  return data_rows.sort((a, b) => getScoreFromTr(a) - getScoreFromTr(b));
};

let data_rows = document.querySelectorAll("#tableSinhVien #tblBody tr");
let data_scores_el = document.querySelectorAll(".td-scores");

let student_list_grade_good = get_student_mark_greater_than_x(
  Array.from(data_rows),
  8
);

let student_list_mark_greater_than_5 = get_student_mark_greater_than_x(
  Array.from(data_rows),
  5
);

let sorted_data_rows = sort_asceding(Array.from(data_rows));

document.querySelectorAll("#svGioiNhat")[0].innerHTML = `${getNameFromTr(
  data_rows[find_max_by_index(data_rows)]
)} - 
  ${getScoreFromTr(data_rows[find_max_by_index(data_rows)])}`;

document.querySelectorAll("#svYeuNhat")[0].innerHTML = `${getNameFromTr(
  data_rows[find_min_by_index(data_rows)]
)} - 
  ${getScoreFromTr(data_rows[find_min_by_index(data_rows)])}`;

let student_list_html = ``;
student_list_mark_greater_than_5.forEach((item) => {
  student_list_html += `<li style='display: block; margin: 0'>${getNameFromTr(
    item
  )} - ${getScoreFromTr(item)}</li>`;
});

let asceding_student_list_html = ``;
sorted_data_rows.forEach((item) => {
  asceding_student_list_html += `<li style='display: block; margin: 0'>${getNameFromTr(
    item
  )} - ${getScoreFromTr(item)}</li>`;
});

document.querySelectorAll(
  "#soSVGioi"
)[0].innerHTML = `${student_list_grade_good.length}`;

document.querySelectorAll(
  "#dsDiemHon5"
)[0].innerHTML = `<ul>${student_list_html}</ul>`;

document.querySelectorAll(
  "#dtbTang"
)[0].innerHTML = `<ul>${asceding_student_list_html}</ul>`;
